<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
    // CONSULTA 1
    public function actionConsulta1a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar edades de los ciclistas (sin repetidos).",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]); 
    }
    
    public function actionConsulta1 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('select count(distinct edad) from ciclista')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar edades de los ciclistas (sin repetidos).",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    
    // CONSULTA 2
    public function actionConsulta2a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("edad")->distinct()->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach.",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
        ]);
       
    }
    
    public function actionConsulta2 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('select count(distinct edad) from ciclista WHERE nomequipo="Artiach"')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar edades de los ciclistas de Artiach.",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'    ",
        ]);
    }
    
    
    // CONSULTA 3
    public function actionConsulta3a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("edad")->distinct()->where("nomequipo='Artiach' OR nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita.",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
       
    }
    
    public function actionConsulta3 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('select count(distinct edad) from ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar edades de los ciclistas de Artiach o de Amore Vita.",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
    }
    
    
    // CONSULTA 4
    public function actionConsulta4a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("dorsal")->distinct()->where("edad<25 OR edad>30"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30",
        ]);
       
    }
    
    public function actionConsulta4 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('SELECT COUNT(DISTINCT dorsal) FROM ciclista WHERE edad<25 OR edad>30')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad<25 OR edad>30",
        ]);
    }


    // CONSULTA 5
    public function actionConsulta5a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()
                ->select("dorsal")->distinct()
                ->where("edad BETWEEN 28 AND 32 AND nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo=Banesto",
        ]);
       
    }
    
    public function actionConsulta5 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto"')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo="Banesto"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo=Banesto",
        ]);
    }
    
    
    // CONSULTA 6 
    public function actionConsulta6a(){
        //Mediante Active Record
        $dataProvider= new ActiveDataProvider([
           'query'=>Ciclista::find()->select("nombre")->distinct()
                ->where("CHAR_LENGTH(nombre)>8"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Listar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8.",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]);
       
    }
    
    public function actionConsulta6 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT nombre) FROM ciclista WHERE CHAR_LENGTH(nombre)>8')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Listar el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8.",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]);
    }
    
    
    // CONSULTA 7 -- INCOMPLETO
    public function actionConsulta7a(){
       
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("upper(nombre) nombremayusculas,dorsal")
                ->distinct()
        ]);
       
       
       
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombremayusculas','dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT UPPER(nombre) nombremayusculas, dorsal FROM ciclista",
           
        ]);
     
    }
    
    public function actionConsulta7 () {
        //Mediante DAO
        $numero=Yii::$app->db->createCommand('SELECT COUNT(DISTINCT dorsal) FROM ciclista')->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT UPPER(nombre) nombremayusculas,dorsal FROM ciclista;',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nombremayusculas','dorsal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT UPPER(nombre) nombremayusculas, dorsal FROM ciclista",
        ]);
    }
    
    // CONSULTA 8 -- PENDIENTE
    public function actionConsulta8a(){
        //Mediante Active Record -- NO FUNCIONA ????
        $dataProvider= new ActiveDataProvider([
           'query' => Lleva::find()
                ->select("dorsal")->distinct()
                ->where("código='MGE'"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>" Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
        ]);
       
    }
    
    public function actionConsulta8 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM lleva WHERE código="MGE"')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT dorsal FROM lleva WHERE código="MGE"',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>" Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
        ]);
    }
    
    
    // CONSULTA 9
    public function actionConsulta9a(){
        //Mediante Active Record - NO FUNCIONA ????
        $dataProvider= new ActiveDataProvider([
           'query' => Puerto::find()
                ->select("nompuerto")->distinct()
                ->where("altura>1500"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
        ]);
       
    }
    
    public function actionConsulta9 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT nompuerto) FROM puerto WHERE altura>1500')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500",
        ]);
    }
    
    
    
    // CONSULTA 10
    public function actionConsulta10a(){
        //Mediante Active Record - NO FUNCIONA ????
        $dataProvider= new ActiveDataProvider([
           'query' => Puerto::find()
                ->select("dorsal")->distinct()
                ->where("pendiente>8 OR altura BETWEEN 1800 AND 3000"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
       
    }
    
    public function actionConsulta10 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    
    // CONSULTA 11
    public function actionConsulta11a(){
        //Mediante Active Record - NO FUNCIONA ????
        $dataProvider= new ActiveDataProvider([
           'query' => Puerto::find()
                ->select("dorsal")->distinct()
                ->where("pendiente>8 AND altura BETWEEN 1800 AND 3000"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
        ]);
       
    }
    
    public function actionConsulta11 () {
        //Mediante DAO
        $numero=Yii::$app->db
                ->createCommand('SELECT COUNT(DISTINCT dorsal) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000')
                ->queryScalar();
        
        $dataProvider= new SqlDataProvider([
           'sql'=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize'=>10,
            ]
            
        ]);
        
        return $this->render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
        ]);
    }
    
}
